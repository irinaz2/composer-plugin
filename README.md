This project needs a new name and a new home (ideally managed by the Drupal Association). This plugin has 2 goals:

- **CI template**. Provide a golden path ([see Spotify's definition of this term](https://engineering.atspotify.com/2020/08/how-we-use-golden-paths-to-solve-fragmentation-in-our-software-ecosystem/)) that Drupal contrib projects can use with Gitlab CI.
- **Local development**. Standardize scripts and a matching codebase assembly for local Drupal development.

Together, these goals ensure that tests run on local environment have same outcome as on the Gitlab CI platform (to a large extent).

### CI
- Add a `.gitlab-ci.yml` in the project root dir. [An example is KeyCDN](https://git.drupalcode.org/project/keycdn/-/blob/8.x-1.x/.gitlab-ci.yml). Commit and push the file.
- Pipelines should start running after each push. Click Gitlab's __CI/CD__ link to see them.
- Add a [phpcs.xml file](https://git.drupalcode.org/project/keycdn/-/blob/8.x-1.x/phpcs.xml) to customize how phpcs checks your source code.
- You may add services to your .gitlab-ci.yml file if you want to test with memcache, elasticsearch, redis, etc.
- Optional. Add patches as per usual in composer.json if you need changes in other packages (for example).
- Optional (maintainers only). Go to CI/CD => Schedules and add any important build permutations to a _weekly_ schedule. Example permutations would be a non-default Drupal core version, DB backend, etc. See _Environment Variables_ below.

### Environment Variables
Use the _Run Pipeline_ button on your Pipelines listing to test any branch with alternate versions of Drupal core, PHP version, or
DB driver. The recognized variables and default values are:

| Name                        | Default |
|-----------------------------|---------|
| DRUPAL_CORE_CONSTRAINT      | ^9      |
| PHP_TAG                     | 7.4     |
| DB_DRIVER                   | mysql   |
| MARIADB_TAG                 | 10.3    |
| POSTGRES_TAG                | 10.5    |
| COMPOSER_PLUGIN_CONSTRAINT  | ^2      |

- DB_DRIVER recognizes `mysql`, `sqlite`, or `pgsql`.

### Local Development
- Clone the contrib project via git
- Install this plugin via `curl -s https://gitlab.com/drupalspoons/composer-plugin/-/raw/master/bin/setup | bash`.
- Configure a web server to serve the `/web` directory as docroot. _Either_ of these works fine:
  - `composer webserver`
  - Setup Apache/Nginx/Other. A virtual host works fine. Any domain name works.
- Configure a database server and create a database.
- Install Drupal `composer si -- --db-url=mysql://user:pass@localhost/db`. Adjust as needed.
- Run tests `composer unit`.
- Customize these composer scripts via environment variables and args/options. [View their definitions](https://gitlab.com/drupalspoons/composer-plugin/-/blob/2.3.0/src/Handler.php#L165-175).

This plugin is compatible with any Docker local development platform like [DDEV](https://ddev.readthedocs.io/en/stable/), [Lando](https://lando.dev/), etc. Further, this plugin doesn't assume Docker so native is great too. A convenient way to set environment variables on native is [Direnv](https://direnv.net/) (and see the .envrc file thats written into your project root). Alternatively, prefix all Composer commands with [spoon](https://gitlab.com/drupalspoons/composer-plugin/-/blob/2.3.0/bin/spoon) like `spoon update` or `spoon drush core:status`

### Handling project changes

- If the project's composer.json changes `composer drupalspoons:prepare`
- If the project adds or removes files or directories in its root `composer drupalspoons:configure`
- If new or updated dependencies are available `composer update`

### More example commands
- Run all tests - `composer unit`.
- Run a suite: `vendor/bin/spoon unit -- --testsuite functional`
- Skip slow tests: `vendor/bin/spoon unit -- --exclude-group slow`
- Use a different URL: `SIMPLETEST_BASE_URL=http://example.com vendor/bin/spoon unit`


### How this plugin works

- This plugin writes a `composer.spoons.json` file that includes the specified version of Drupal core.
- The plugin assumes that `composer.spoons.json` is in effect when running `composer` commands. This is done automatically for CI. For local development,  `COMPOSER=composer.spoons.json` environment variable must be set. You can do that manually or via [Direnv](https://direnv.net/), or [Docker](https://docs.docker.com/compose/env-file/), or [spoon](https://gitlab.com/drupalspoons/composer-plugin/-/blob/2.3.0/bin/spoon).
- This plugin assembles a codebase using symlinks similar to:

![Folder tree](/assets/folder.png)
